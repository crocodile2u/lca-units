<?php

namespace LCAUnits;

class FlowPropertySpec implements \JsonSerializable
{
    private string $name;
    private string $unitGroupName;
    /**
     * @var string[]
     */
    private array $units;

    /**
     * FlowPropertySpec constructor.
     * @param string $name
     * @param string $unitGroupName
     * @param string[] $units
     */
    public function __construct(string $name, string $unitGroupName, string ...$units)
    {
        $this->name = $name;
        $this->unitGroupName = $unitGroupName;
        $this->units = $units;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function unitGroupName(): string
    {
        return $this->unitGroupName;
    }

    /**
     * @return string[]
     */
    public function units(): array
    {
        return $this->units;
    }

    public function jsonSerialize()
    {
        return [
            "name" => $this->name,
            "unit_group" => $this->unitGroupName,
            "base_unit" => UnitGroup::REGISTRY[$this->unitGroupName],
            "units" => $this->units,
        ];
    }
}