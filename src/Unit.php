<?php
declare(strict_types=1);

namespace LCAUnits;

class Unit implements \JsonSerializable
{
    const DOZENS_OF_ITEMS = 12;
    const NUMBER_OF_ITEMS = 1;
    const CENTIMETRE_TIMES_SQUARE_METRE_PER_DAY = 1;
    const ITEMS_TIMES_KILOMETRE = 1;
    const ITEMS_TIMES_NAUTICAL_MILE = 1.85;
    const ITEMS_TIMES_INTERNATIONAL_MILE = 1.6;
    const LITRE_TIMES_YEAR = 0.001;
    const LITRE_TIMES_DAY = 2.74E-6;
    const CUBIC_CENTIMETRES_TIMES_YEAR = 1.0E-6;
    const CUBIC_METRE_TIMES_YEAR = 1;
    const CUBIC_METRE_TIMES_DAY = 0.00274;
    const MILLIMETRE_TIMES_SQUARE_METRE = 1;
    const METRIC_TONNES_TIMES_YEAR = 1000;
    const GRAM_TIMES_YEAR = 0.001;
    const METRIC_TONNE_TIMES_DAY_1_YEAR_365_DAYS = 2.73;
    const KILOGRAM_TIMES_DAY_1_YEAR_365_DAYS = 0.00274;
    const KILOGRAM_TIMES_YEAR = 1;
    const MILLIMETRE_TIMES_SQUARE_METRE_PER_YEAR = 1;
    const LITRE_TIMES_NAUTICAL_MILE = 0.00185;
    const LITRE_TIMES_INTERNATIONAL_MILE = 0.00161;
    const CUBIC_METRE_TIMES_KILOMETRE = 1;
    const LITRE_TIMES_KILOMETRE = 0.001;
    const CUBIC_METRE_TIMES_INTERNATIONAL_MILE = 1.6;
    const CUBIC_METRE_TIMES_NAUTICAL_MILE = 1.85;
    const KILOTONNE_TIMES_KILOMETRE = 1000;
    const METRIC_TONNEKILOMETRE = 1;
    const METRIC_TONNE_TIMES_INTERNATIONAL_MILE = 1.609344;
    const BRITISH_POUND_AVOIRDUPOIS_TIMES_INTERNATIONAL_MILE = 0.00073;
    const METRIC_TONNE_TIMES_NAUTICAL_MILE = 1.85;
    const KILOGRAMKILOMETRE = 0.001;
    const BRITISH_POUND_AVOIRDUPOIS_TIMES_NAUTICAL_MILE = 0.00084;
    const INCH = 0.02;
    const HECTOMETRE = 100;
    const FURLONG = 201.168;
    const CHAIN = 20.1168;
    const NAUTICAL_MILE = 1852;
    const HAND = 0.1016;
    const FATHOM = 1.8288;
    const DECIMETRE = 0.1;
    const METRE = 1;
    const INTERNATIONAL_MILE = 1609.34;
    const KILOMETRE = 1000;
    const MICRON = 1.0E-6;
    const DECAMETRE = 10;
    const YARD_INTERNATIONAL = 0.91;
    const MILLIMETRE = 0.001;
    const CENTIMETRE = 0.01;
    const FOOT_INTERNATIONAL = 0.3;
    const PERSON_TIMES_MILE = 1.6;
    const PERSON_KILOMETRE = 1;
    const CENTIMOLE_TIMES_SQUARE_METRE_TIMES_YEAR_PER_KILOGRAM = 1;
    const BRITISH_POUND_AVOIRDUPOIS = 0.45;
    const KILOGRAM = 1;
    const CENTIGRAM = 1.0E-5;
    const OUNCE_TROY = 0.03;
    const SHORT_HUNDREDWEIGHT = 45.359237;
    const DECIGRAM = 0.0001;
    const DRAM_AVOIRDUPOIS = 0.001771845;
    const NANOGRAM = 1.0E-12;
    const PICOGRAM_1012_G = 1.0E-15;
    const GRAIN = 6.48E-5;
    const TONNE = 1000;
    const HECTOGRAM = 0.1;
    const DECAGRAM = 0.01;
    const KILOGRAM_SWU = 1;
    const MEGATONNE = 1000000000;
    const PENNYWEIGHT = 0.001555174;
    const CARAT_METRIC = 0.0002;
    const MILLIGRAM = 1.0E-6;
    const MICROGRAM = 1.0E-9;
    const MEGAGRAM_1_METRIC_TONNE = 1000;
    const SHORT_TON = 907.18;
    const GRAM = 0.001;
    const LONG_TON = 1016.04;
    const KILOTONNE = 1000000;
    const OUNCE_AVOIRDUPOIS_COMMONLY_USED_BUT_NOT_FOR_GOLD_PLATINUM_ETC_SEE_OUNCE_TROY = 0.02;
    const RUTHERFORD = 1000;
    const MICROBEQUEREL = 1.0E-9;
    const BEQUEREL_1_EVENT_PER_SECOND = 0.001;
    const NANOBEQUEREL = 1.0E-12;
    const MILIBEQUEREL = 1.0E-6;
    const KILOBEQUEREL_1000_EVENTS_PER_SECOND = 1;
    const CURIE = 37000000;
    const SQUARE_MILLIMETRES = 1.0E-6;
    const BRITISH_SQUARE_MILE = 2589988.11;
    const SQUARE_METRE = 1;
    const SQUARE_NAUTICAL_MILE = 3429904;
    const SQUARE_CENTIMETRE = 0.0001;
    const SQUARE_YARD_IMPERIAL_PER_US = 0.83;
    const ACRE_US_SURVEY = 4046.85;
    const SQUARE_KILOMETRE = 1000000;
    const BRITISH_SQUARE_FEET = 0.09;
    const SQUARE_INCHES = 0.000645;
    const SQUARE_DECIMETRES = 0.01;
    const HECTARE = 10000;
    const ARE = 100;
    const SQUARE_METRE_TIMES_DAY = 0.00274;
    const SQUARE_CENTIMETRES_TIMES_YEAR = 0.0001;
    const BRITISH_SQUARE_MILE_TIMES_YEAR = 2589988.1;
    const HECTARE_TIMES_YEAR = 10000;
    const SQUARE_MILLIMETRES_TIMES_YEAR = 1.0E-6;
    const SQUARE_METRE_TIMES_YEAR = 1;
    const SQUARE_KILOMETRE_TIMES_YEAR = 1000000;
    const BRITISH_SQUARE_FEET_TIMES_YEAR = 0.09;
    const GILL = 0.000118294;
    const CUBIC_FEET = 0.02;
    const QUART_US_DRY = 0.001101221;
    const CUBIC_METRE = 1;
    const BARREL_US_DRY = 0.11;
    const US_CUSTOMARY_FLUID_OUNCE = 2.96E-5;
    const DECALITRE = 0.01;
    const PINT_US_FLUID = 0.000473;
    const CUBIC_DECIMETER = 0.001;
    const GALLON_US_FLUID_USED_IN_US_EG_FOR_FUEL = 0.00379;
    const NORMAL_CUBIC_METRES = 1;
    const CUBIC_CENTIMETRES = 1.0E-6;
    const CUBIC_MILLIMETRES = 1.0E-9;
    const US_BUSHEL = 0.03;
    const DECILITRE = 0.0001;
    const GALLON_US_LIQUID = 0.00379;
    const CUBIC_INCHES = 1.64E-5;
    const DRAM_FLUID = 3.7E-6;
    const PECK = 0.008809768;
    const FLUID_OUNCE_IMPERIAL = 2.84E-5;
    const MILLILITRE = 1.0E-6;
    const BARREL_US_NONBEER_FLUID = 0.11;
    const GALLON_IMPERIAL_USED_IN_UK_UNITED_ARAB_EMIRATES_FOR_FUELS = 0.00455;
    const IMPERIAL_BUSHEL = 0.03;
    const BARREL_PETROLEUM = 0.15;
    const BARREL_IMPERIAL = 0.16;
    const CUBIC_YARDS = 0.76;
    const PINT_US_DRY = 0.000551;
    const BARREL_US_BEER = 0.11;
    const CENTILITRE = 1.0E-5;
    const LITRE = 0.001;
    const PINT_IMPERIAL = 0.000568;
    const GALLON_US_DRY = 0.0044;
    const MINIM = 6.16E-8;
    const MICROLITRE = 1.0E-9;
    const HECTOLITRE = 0.1;
    const QUART_US_LIQUID = 0.000946353;
    const CENTIMOLE_TIMES_SQUARE_METRE_PER_KILOGRAM = 1;
    const MILES_TIMES_YEAR = 1609.35;
    const KILOMETRES_TIMES_YEAR = 1000;
    const METRE_TIMES_YEAR = 1;
    const DAY = 1;
    const HOUR = 0.041666666666667;
    const SECOND = 1.16E-5;
    const YEAR_ROUNDED = 365;
    const MINUTE = 0.000694;
    const MEGAJOULE_PER_KILOGRAM_TIMES_DAY = 1;
    const CENTIMETRE_TIMES_CUBIC_METRE = 1;
    const KILOWATTHOUR_PER_SQUARE_METRE_TIMES_DAY = 1;
    const VEHICLEKILOMETER = 1;
    const KILOCALORIE_INTERNATIONAL_TABLE = 0.00419;
    const GIGAJOULE = 1000;
    const TONNE_OIL_EQUIVALENT = 41868;
    const JOULE = 1.0E-6;
    const MEGAJOULE = 1;
    const BRITISH_THERMAL_UNIT_INTERNATIONAL_TABLE = 0.00106;
    const TERAJOULE = 1000000;
    const TONNE_COAL_EQUIVALENT = 29307.6;
    const KILOWATT_TIMES_HOUR = 3.6;
    const MEGAWATT_TIMES_HOUR = 3600;
    const KILOJOULE = 0.001;
    const WATT_TIMES_HOUR = 0.0036;
    const PETAJOULES = 1000000000;
    const KILOGRAM_PER_YEAR = 1;

    const REGISTRY = [
        'dozen(s)' => [UnitGroup::ITEMS, self::DOZENS_OF_ITEMS],
        'dozen' => [UnitGroup::ITEMS, self::DOZENS_OF_ITEMS],
        'Dozen(s)' => [UnitGroup::ITEMS, self::DOZENS_OF_ITEMS],
        'Item(s)' => [UnitGroup::ITEMS, self::NUMBER_OF_ITEMS],
        'item(s)' => [UnitGroup::ITEMS, self::NUMBER_OF_ITEMS],
        'item' => [UnitGroup::ITEMS, self::NUMBER_OF_ITEMS],
        'unit' => [UnitGroup::ITEMS, self::NUMBER_OF_ITEMS],
        '' => [UnitGroup::ITEMS, self::NUMBER_OF_ITEMS],
        'LU' => [UnitGroup::ITEMS, self::NUMBER_OF_ITEMS],
        'pig place' => [UnitGroup::ITEMS, self::NUMBER_OF_ITEMS],
        'p' => [UnitGroup::ITEMS, self::NUMBER_OF_ITEMS],
        'cm*m2/d' => [UnitGroup::LENGTH_TIMES_AREA_PER_TIME, self::CENTIMETRE_TIMES_SQUARE_METRE_PER_DAY],
        'Items*km' => [UnitGroup::ITEMS_TIMES_LENGTH, self::ITEMS_TIMES_KILOMETRE],
        'Items*nmi' => [UnitGroup::ITEMS_TIMES_LENGTH, self::ITEMS_TIMES_NAUTICAL_MILE],
        'Items*mi' => [UnitGroup::ITEMS_TIMES_LENGTH, self::ITEMS_TIMES_INTERNATIONAL_MILE],
        'l*a' => [UnitGroup::VOLUME_TIMES_TIME, self::LITRE_TIMES_YEAR],
        'l*d' => [UnitGroup::VOLUME_TIMES_TIME, self::LITRE_TIMES_DAY],
        'l*day' => [UnitGroup::VOLUME_TIMES_TIME, self::LITRE_TIMES_DAY],
        'cm3*a' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_CENTIMETRES_TIMES_YEAR],
        'cm3y' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_CENTIMETRES_TIMES_YEAR],
        'm3*a' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_METRE_TIMES_YEAR],
        'm³*a' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_METRE_TIMES_YEAR],
        'm3a' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_METRE_TIMES_YEAR],
        'm3y' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_METRE_TIMES_YEAR],
        'm3*d' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_METRE_TIMES_DAY],
        'm³*d' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_METRE_TIMES_DAY],
        'm3d' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_METRE_TIMES_DAY],
        'm3day' => [UnitGroup::VOLUME_TIMES_TIME, self::CUBIC_METRE_TIMES_DAY],
        'mm*m2' => [UnitGroup::LENGTH_TIMES_AREA, self::MILLIMETRE_TIMES_SQUARE_METRE],
        't*a' => [UnitGroup::MASS_TIMES_TIME, self::METRIC_TONNES_TIMES_YEAR],
        'g*a' => [UnitGroup::MASS_TIMES_TIME, self::GRAM_TIMES_YEAR],
        't*d' => [UnitGroup::MASS_TIMES_TIME, self::METRIC_TONNE_TIMES_DAY_1_YEAR_365_DAYS],
        'kg*d' => [UnitGroup::MASS_TIMES_TIME, self::KILOGRAM_TIMES_DAY_1_YEAR_365_DAYS],
        'kg*a' => [UnitGroup::MASS_TIMES_TIME, self::KILOGRAM_TIMES_YEAR],
        'kgy' => [UnitGroup::MASS_TIMES_TIME, self::KILOGRAM_TIMES_YEAR],
        '(mm*m2)/a' => [UnitGroup::GROUNDWATER_REPLENISHMENT_TRANSF, self::MILLIMETRE_TIMES_SQUARE_METRE_PER_YEAR],
        'l*nmi' => [UnitGroup::VOLUME_TIMES_LENGTH, self::LITRE_TIMES_NAUTICAL_MILE],
        'l*mi' => [UnitGroup::VOLUME_TIMES_LENGTH, self::LITRE_TIMES_INTERNATIONAL_MILE],
        'm3*km' => [UnitGroup::VOLUME_TIMES_LENGTH, self::CUBIC_METRE_TIMES_KILOMETRE],
        'l*km' => [UnitGroup::VOLUME_TIMES_LENGTH, self::LITRE_TIMES_KILOMETRE],
        'm3*mi' => [UnitGroup::VOLUME_TIMES_LENGTH, self::CUBIC_METRE_TIMES_INTERNATIONAL_MILE],
        'm3*nmi' => [UnitGroup::VOLUME_TIMES_LENGTH, self::CUBIC_METRE_TIMES_NAUTICAL_MILE],
        'kt*km' => [UnitGroup::MASS_TIMES_LENGTH, self::KILOTONNE_TIMES_KILOMETRE],
        'ktkm' => [UnitGroup::MASS_TIMES_LENGTH, self::KILOTONNE_TIMES_KILOMETRE],
        't*km' => [UnitGroup::MASS_TIMES_LENGTH, self::METRIC_TONNEKILOMETRE],
        'tkm' => [UnitGroup::MASS_TIMES_LENGTH, self::METRIC_TONNEKILOMETRE],
        't*mi' => [UnitGroup::MASS_TIMES_LENGTH, self::METRIC_TONNE_TIMES_INTERNATIONAL_MILE],
        'lb*mi' => [UnitGroup::MASS_TIMES_LENGTH, self::BRITISH_POUND_AVOIRDUPOIS_TIMES_INTERNATIONAL_MILE],
        't*nmi' => [UnitGroup::MASS_TIMES_LENGTH, self::METRIC_TONNE_TIMES_NAUTICAL_MILE],
        'kg*km' => [UnitGroup::MASS_TIMES_LENGTH, self::KILOGRAMKILOMETRE],
        'kgkm' => [UnitGroup::MASS_TIMES_LENGTH, self::KILOGRAMKILOMETRE],
        'lb*nmi' => [UnitGroup::MASS_TIMES_LENGTH, self::BRITISH_POUND_AVOIRDUPOIS_TIMES_NAUTICAL_MILE],
        'in' => [UnitGroup::LENGTH, self::INCH],
        'inch' => [UnitGroup::LENGTH, self::INCH],
        'hm' => [UnitGroup::LENGTH, self::HECTOMETRE],
        'fur' => [UnitGroup::LENGTH, self::FURLONG],
        'ch' => [UnitGroup::LENGTH, self::CHAIN],
        'nmi' => [UnitGroup::LENGTH, self::NAUTICAL_MILE],
        'hh' => [UnitGroup::LENGTH, self::HAND],
        'ftm' => [UnitGroup::LENGTH, self::FATHOM],
        'dm' => [UnitGroup::LENGTH, self::DECIMETRE],
        'm' => [UnitGroup::LENGTH, self::METRE],
        'mi' => [UnitGroup::LENGTH, self::INTERNATIONAL_MILE],
        'mile' => [UnitGroup::LENGTH, self::INTERNATIONAL_MILE],
        'km' => [UnitGroup::LENGTH, self::KILOMETRE],
        'u' => [UnitGroup::LENGTH, self::MICRON],
        'µm' => [UnitGroup::LENGTH, self::MICRON],
        'um' => [UnitGroup::LENGTH, self::MICRON],
        'dam' => [UnitGroup::LENGTH, self::DECAMETRE],
        'yd' => [UnitGroup::LENGTH, self::YARD_INTERNATIONAL],
        'yard' => [UnitGroup::LENGTH, self::YARD_INTERNATIONAL],
        'mm' => [UnitGroup::LENGTH, self::MILLIMETRE],
        'cm' => [UnitGroup::LENGTH, self::CENTIMETRE],
        'ft' => [UnitGroup::LENGTH, self::FOOT_INTERNATIONAL],
        'p*mi' => [UnitGroup::PERSON_TRANSPORT, self::PERSON_TIMES_MILE],
        'pmi' => [UnitGroup::PERSON_TRANSPORT, self::PERSON_TIMES_MILE],
        'p*km' => [UnitGroup::PERSON_TRANSPORT, self::PERSON_KILOMETRE],
        'pkm' => [UnitGroup::PERSON_TRANSPORT, self::PERSON_KILOMETRE],
        'personkm' => [UnitGroup::PERSON_TRANSPORT, self::PERSON_KILOMETRE],
        '(cmol*m2*a)/kg' => [UnitGroup::MOLE_TIMES_AREA_TIMES_TIME_PER_MASS, self::CENTIMOLE_TIMES_SQUARE_METRE_TIMES_YEAR_PER_KILOGRAM],
        'lb av' => [UnitGroup::MASS, self::BRITISH_POUND_AVOIRDUPOIS],
        'lb' => [UnitGroup::MASS, self::BRITISH_POUND_AVOIRDUPOIS],
        'kg' => [UnitGroup::MASS, self::KILOGRAM],
        'cg' => [UnitGroup::MASS, self::CENTIGRAM],
        'oz t' => [UnitGroup::MASS, self::OUNCE_TROY],
        'cwt' => [UnitGroup::MASS, self::SHORT_HUNDREDWEIGHT],
        'dg' => [UnitGroup::MASS, self::DECIGRAM],
        'dr (Av)' => [UnitGroup::MASS, self::DRAM_AVOIRDUPOIS],
        'ng' => [UnitGroup::MASS, self::NANOGRAM],
        'pg' => [UnitGroup::MASS, self::PICOGRAM_1012_G],
        'gr' => [UnitGroup::MASS, self::GRAIN],
        't' => [UnitGroup::MASS, self::TONNE],
        'Mg' => [UnitGroup::MASS, self::MEGAGRAM_1_METRIC_TONNE],
        'ton' => [UnitGroup::MASS, self::TONNE],
        'hg' => [UnitGroup::MASS, self::HECTOGRAM],
        'dag' => [UnitGroup::MASS, self::DECAGRAM],
        'kg SWU' => [UnitGroup::MASS, self::KILOGRAM_SWU],
        'Mt' => [UnitGroup::MASS, self::MEGATONNE],
        'Mtn' => [UnitGroup::MASS, self::MEGATONNE],
        'dwt' => [UnitGroup::MASS, self::PENNYWEIGHT],
        'ct' => [UnitGroup::MASS, self::CARAT_METRIC],
        'carat' => [UnitGroup::MASS, self::CARAT_METRIC],
        'mg' => [UnitGroup::MASS, self::MILLIGRAM],
        'ug' => [UnitGroup::MASS, self::MICROGRAM],
        'µg' => [UnitGroup::MASS, self::MICROGRAM],
        'sh tn' => [UnitGroup::MASS, self::SHORT_TON],
        'tn.sh' => [UnitGroup::MASS, self::SHORT_TON],
        'g' => [UnitGroup::MASS, self::GRAM],
        'long tn' => [UnitGroup::MASS, self::LONG_TON],
        'tn.lg' => [UnitGroup::MASS, self::LONG_TON],
        'kt' => [UnitGroup::MASS, self::KILOTONNE],
        'oz av' => [UnitGroup::MASS, self::OUNCE_AVOIRDUPOIS_COMMONLY_USED_BUT_NOT_FOR_GOLD_PLATINUM_ETC_SEE_OUNCE_TROY],
        'oz' => [UnitGroup::MASS, self::OUNCE_AVOIRDUPOIS_COMMONLY_USED_BUT_NOT_FOR_GOLD_PLATINUM_ETC_SEE_OUNCE_TROY],
        'Rutherford' => [UnitGroup::RADIOACTIVITY, self::RUTHERFORD],
        'µBq' => [UnitGroup::RADIOACTIVITY, self::MICROBEQUEREL],
        'Bq' => [UnitGroup::RADIOACTIVITY, self::BEQUEREL_1_EVENT_PER_SECOND],
        'nBq' => [UnitGroup::RADIOACTIVITY, self::NANOBEQUEREL],
        'mBq' => [UnitGroup::RADIOACTIVITY, self::MILIBEQUEREL],
        'kBq' => [UnitGroup::RADIOACTIVITY, self::KILOBEQUEREL_1000_EVENTS_PER_SECOND],
        'Ci' => [UnitGroup::RADIOACTIVITY, self::CURIE],
        'mm2' => [UnitGroup::AREA, self::SQUARE_MILLIMETRES],
        'mi2' => [UnitGroup::AREA, self::BRITISH_SQUARE_MILE],
        'mi²' => [UnitGroup::AREA, self::BRITISH_SQUARE_MILE],
        'sq.mi' => [UnitGroup::AREA, self::BRITISH_SQUARE_MILE],
        'm2' => [UnitGroup::AREA, self::SQUARE_METRE],
        'm²' => [UnitGroup::AREA, self::SQUARE_METRE],
        'nmi2' => [UnitGroup::AREA, self::SQUARE_NAUTICAL_MILE],
        'cm2' => [UnitGroup::AREA, self::SQUARE_CENTIMETRE],
        'yd2' => [UnitGroup::AREA, self::SQUARE_YARD_IMPERIAL_PER_US],
        'sq.yd' => [UnitGroup::AREA, self::SQUARE_YARD_IMPERIAL_PER_US],
        'ac' => [UnitGroup::AREA, self::ACRE_US_SURVEY],
        'acre (US)' => [UnitGroup::AREA, self::ACRE_US_SURVEY],
        'acre' => [UnitGroup::AREA, self::ACRE_US_SURVEY],
        'km2' => [UnitGroup::AREA, self::SQUARE_KILOMETRE],
        'km²' => [UnitGroup::AREA, self::SQUARE_KILOMETRE],
        'ft2' => [UnitGroup::AREA, self::BRITISH_SQUARE_FEET],
        'ft²' => [UnitGroup::AREA, self::BRITISH_SQUARE_FEET],
        'sq.ft' => [UnitGroup::AREA, self::BRITISH_SQUARE_FEET],
        'in2' => [UnitGroup::AREA, self::SQUARE_INCHES],
        'sq.in' => [UnitGroup::AREA, self::SQUARE_INCHES],
        'dm2' => [UnitGroup::AREA, self::SQUARE_DECIMETRES],
        'ha' => [UnitGroup::AREA, self::HECTARE],
        'are' => [UnitGroup::AREA, self::ARE],
        'm2*d' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_METRE_TIMES_DAY],
        'cm2a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_CENTIMETRES_TIMES_YEAR],
        'mi2*a' => [UnitGroup::AREA_TIMES_TIME, self::BRITISH_SQUARE_MILE_TIMES_YEAR],
        'mi²*a' => [UnitGroup::AREA_TIMES_TIME, self::BRITISH_SQUARE_MILE_TIMES_YEAR],
        'mi2a' => [UnitGroup::AREA_TIMES_TIME, self::BRITISH_SQUARE_MILE_TIMES_YEAR],
        'mi²a' => [UnitGroup::AREA_TIMES_TIME, self::BRITISH_SQUARE_MILE_TIMES_YEAR],
        'ha*a' => [UnitGroup::AREA_TIMES_TIME, self::HECTARE_TIMES_YEAR],
        'ha a' => [UnitGroup::AREA_TIMES_TIME, self::HECTARE_TIMES_YEAR],
        'mm2a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_MILLIMETRES_TIMES_YEAR],
        'm2*a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_METRE_TIMES_YEAR],
        'm²*a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_METRE_TIMES_YEAR],
        'm2a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_METRE_TIMES_YEAR],
        'm²a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_METRE_TIMES_YEAR],
        'm2*year' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_METRE_TIMES_YEAR],
        'km2*a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_KILOMETRE_TIMES_YEAR],
        'km²*a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_KILOMETRE_TIMES_YEAR],
        'km2a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_KILOMETRE_TIMES_YEAR],
        'km²a' => [UnitGroup::AREA_TIMES_TIME, self::SQUARE_KILOMETRE_TIMES_YEAR],
        'ft2*a' => [UnitGroup::AREA_TIMES_TIME, self::BRITISH_SQUARE_FEET_TIMES_YEAR],
        'ft²*a' => [UnitGroup::AREA_TIMES_TIME, self::BRITISH_SQUARE_FEET_TIMES_YEAR],
        'ft²a' => [UnitGroup::AREA_TIMES_TIME, self::BRITISH_SQUARE_FEET_TIMES_YEAR],
        'ft2a' => [UnitGroup::AREA_TIMES_TIME, self::BRITISH_SQUARE_FEET_TIMES_YEAR],
        'gill' => [UnitGroup::VOLUME, self::GILL],
        'cu ft' => [UnitGroup::VOLUME, self::CUBIC_FEET],
        'cuft' => [UnitGroup::VOLUME, self::CUBIC_FEET],
        'qt (US dry)' => [UnitGroup::VOLUME, self::QUART_US_DRY],
        'm3' => [UnitGroup::VOLUME, self::CUBIC_METRE],
        'm³' => [UnitGroup::VOLUME, self::CUBIC_METRE],
        'kl' => [UnitGroup::VOLUME, self::CUBIC_METRE],
        'kL' => [UnitGroup::VOLUME, self::CUBIC_METRE],
        'bl (US dry)' => [UnitGroup::VOLUME, self::BARREL_US_DRY],
        'US fl oz' => [UnitGroup::VOLUME, self::US_CUSTOMARY_FLUID_OUNCE],
        'dal' => [UnitGroup::VOLUME, self::DECALITRE],
        'daL' => [UnitGroup::VOLUME, self::DECALITRE],
        'pt (US fl)' => [UnitGroup::VOLUME, self::PINT_US_FLUID],
        'dm3' => [UnitGroup::VOLUME, self::CUBIC_DECIMETER],
        'gal (US fl)' => [UnitGroup::VOLUME, self::GALLON_US_FLUID_USED_IN_US_EG_FOR_FUEL],
        'Nm3' => [UnitGroup::VOLUME, self::NORMAL_CUBIC_METRES],
        'Nm³' => [UnitGroup::VOLUME, self::NORMAL_CUBIC_METRES],
        'cm3' => [UnitGroup::VOLUME, self::CUBIC_CENTIMETRES],
        'mm3' => [UnitGroup::VOLUME, self::CUBIC_MILLIMETRES],
        'bsh (US)' => [UnitGroup::VOLUME, self::US_BUSHEL],
        'dl' => [UnitGroup::VOLUME, self::DECILITRE],
        'dL' => [UnitGroup::VOLUME, self::DECILITRE],
        'gal (US liq)' => [UnitGroup::VOLUME, self::GALLON_US_LIQUID],
        'gal*' => [UnitGroup::VOLUME, self::GALLON_US_LIQUID],
        'in3' => [UnitGroup::VOLUME, self::CUBIC_INCHES],
        'cu.in' => [UnitGroup::VOLUME, self::CUBIC_INCHES],
        'dr (Fl)' => [UnitGroup::VOLUME, self::DRAM_FLUID],
        'pk' => [UnitGroup::VOLUME, self::PECK],
        'fl oz (Imp)' => [UnitGroup::VOLUME, self::FLUID_OUNCE_IMPERIAL],
        'ml' => [UnitGroup::VOLUME, self::MILLILITRE],
        'mL' => [UnitGroup::VOLUME, self::MILLILITRE],
        'bl (US fl)' => [UnitGroup::VOLUME, self::BARREL_US_NONBEER_FLUID],
        'gal (Imp)' => [UnitGroup::VOLUME, self::GALLON_IMPERIAL_USED_IN_UK_UNITED_ARAB_EMIRATES_FOR_FUELS],
        'bsh (Imp)' => [UnitGroup::VOLUME, self::IMPERIAL_BUSHEL],
        'bbl' => [UnitGroup::VOLUME, self::BARREL_PETROLEUM],
        'bl (Imp)' => [UnitGroup::VOLUME, self::BARREL_IMPERIAL],
        'yd3' => [UnitGroup::VOLUME, self::CUBIC_YARDS],
        'cu.yd' => [UnitGroup::VOLUME, self::CUBIC_YARDS],
        'pt (US dry)' => [UnitGroup::VOLUME, self::PINT_US_DRY],
        'bl (US beer)' => [UnitGroup::VOLUME, self::BARREL_US_BEER],
        'cl' => [UnitGroup::VOLUME, self::CENTILITRE],
        'cL' => [UnitGroup::VOLUME, self::CENTILITRE],
        'l' => [UnitGroup::VOLUME, self::LITRE],
        'pt (Imp)' => [UnitGroup::VOLUME, self::PINT_IMPERIAL],
        'gal (US dry)' => [UnitGroup::VOLUME, self::GALLON_US_DRY],
        'Imp.min.' => [UnitGroup::VOLUME, self::MINIM],
        'ul' => [UnitGroup::VOLUME, self::MICROLITRE],
        'µl' => [UnitGroup::VOLUME, self::MICROLITRE],
        'µL' => [UnitGroup::VOLUME, self::MICROLITRE],
        'hl' => [UnitGroup::VOLUME, self::HECTOLITRE],
        'hL' => [UnitGroup::VOLUME, self::HECTOLITRE],
        'qt (US liq)' => [UnitGroup::VOLUME, self::QUART_US_LIQUID],
        '(cmol*m2)/kg' => [UnitGroup::MOLE_TIMES_AREA_PER_MASS, self::CENTIMOLE_TIMES_SQUARE_METRE_PER_KILOGRAM],
        'mi*a' => [UnitGroup::LENGTH_TIMES_TIME, self::MILES_TIMES_YEAR],
        'miy' => [UnitGroup::LENGTH_TIMES_TIME, self::MILES_TIMES_YEAR],
        'km*a' => [UnitGroup::LENGTH_TIMES_TIME, self::KILOMETRES_TIMES_YEAR],
        'kmy' => [UnitGroup::LENGTH_TIMES_TIME, self::KILOMETRES_TIMES_YEAR],
        'm*a' => [UnitGroup::LENGTH_TIMES_TIME, self::METRE_TIMES_YEAR],
        'ma' => [UnitGroup::LENGTH_TIMES_TIME, self::METRE_TIMES_YEAR],
        'my' => [UnitGroup::LENGTH_TIMES_TIME, self::METRE_TIMES_YEAR],
        'd' => [UnitGroup::TIME, self::DAY],
        'day' => [UnitGroup::TIME, self::DAY],
        'h' => [UnitGroup::TIME, self::HOUR],
        'hr' => [UnitGroup::TIME, self::HOUR],
        's' => [UnitGroup::TIME, self::SECOND],
        'a' => [UnitGroup::TIME, self::YEAR_ROUNDED],
        'year' => [UnitGroup::TIME, self::YEAR_ROUNDED],
        'yr' => [UnitGroup::TIME, self::YEAR_ROUNDED],
        'min' => [UnitGroup::TIME, self::MINUTE],
        'MJ/kg*d' => [UnitGroup::ENERGY_PER_MASS_TIMES_TIME, self::MEGAJOULE_PER_KILOGRAM_TIMES_DAY],
        'cm*m3' => [UnitGroup::MECHANICAL_FILTRATION_OCC, self::CENTIMETRE_TIMES_CUBIC_METRE],
        'kWh/m2*d' => [UnitGroup::ENERGY_PER_AREA_TIMES_TIME, self::KILOWATTHOUR_PER_SQUARE_METRE_TIMES_DAY],
        'kWh/m2d' => [UnitGroup::ENERGY_PER_AREA_TIMES_TIME, self::KILOWATTHOUR_PER_SQUARE_METRE_TIMES_DAY],
        'kWh/m²d' => [UnitGroup::ENERGY_PER_AREA_TIMES_TIME, self::KILOWATTHOUR_PER_SQUARE_METRE_TIMES_DAY],
        'v*km' => [UnitGroup::VEHICLE_TRANSPORT, self::VEHICLEKILOMETER],
        'vkm' => [UnitGroup::VEHICLE_TRANSPORT, self::VEHICLEKILOMETER],
        'kcal' => [UnitGroup::ENERGY, self::KILOCALORIE_INTERNATIONAL_TABLE],
        'GJ' => [UnitGroup::ENERGY, self::GIGAJOULE],
        'TOE' => [UnitGroup::ENERGY, self::TONNE_OIL_EQUIVALENT],
        'J' => [UnitGroup::ENERGY, self::JOULE],
        'MJ' => [UnitGroup::ENERGY, self::MEGAJOULE],
        'btu' => [UnitGroup::ENERGY, self::BRITISH_THERMAL_UNIT_INTERNATIONAL_TABLE],
        'TJ' => [UnitGroup::ENERGY, self::TERAJOULE],
        'TCE' => [UnitGroup::ENERGY, self::TONNE_COAL_EQUIVALENT],
        'kWh' => [UnitGroup::ENERGY, self::KILOWATT_TIMES_HOUR],
        'MWh' => [UnitGroup::ENERGY, self::MEGAWATT_TIMES_HOUR],
        'kJ' => [UnitGroup::ENERGY, self::KILOJOULE],
        'Wh' => [UnitGroup::ENERGY, self::WATT_TIMES_HOUR],
        'PJ' => [UnitGroup::ENERGY, self::PETAJOULES],
        'kg/a' => [UnitGroup::MASS_PER_TIME, self::KILOGRAM_PER_YEAR],
    ];

    private string $name;

    public static function spec(string $unit): array
    {
        return self::REGISTRY[$unit] ?? ["", 1.0];
    }

    /**
     * @return self[][]
     */
    public static function unitsByGroups(): array
    {
        return self::groups(true);
    }

    /**
     * @return string[][]
     */
    public static function unitNamesByGroups(): array
    {
        return self::groups(false);
    }

    protected static function groups(bool $unitsAsObjects): array
    {
        $ret = [];
        foreach (self::REGISTRY as $unitName => $spec) {
            [$group] = $spec;
            $ret[$group][] = $unitsAsObjects ? new self($unitName) : $unitName;
        }
        return $ret;
    }

    /**
     * Unit constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = trim($name);
    }

    public function equals(self $other): bool
    {
        return $this->name === $other->name;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function group(): UnitGroup
    {
        return new UnitGroup(self::spec($this->name)[0]);
    }

    public function factor(): float
    {
        return self::spec($this->name)[1];
    }

    public function jsonSerialize()
    {
        return [
            "name" => $this->name,
            "group" => $this->group(),
            "factor" => $this->factor(),
        ];
    }
}