-- runs on a derby DB which I made new from openLCA GUI, using the "complete reference data" option
select u.NAME, u.SYNONYMS, u.DESCRIPTION, u.CONVERSION_FACTOR, ug.NAME UNIT_GROUP, p.NAME PROPERTY
from TBL_UNITS u
         join TBL_UNIT_GROUPS ug ON (ug.ID = u.F_UNIT_GROUP)
         join TBL_FLOW_PROPERTIES p ON (p.ID = ug.F_DEFAULT_FLOW_PROPERTY)
where p.FLOW_PROPERTY_TYPE != '0'