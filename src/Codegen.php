<?php

namespace LCAUnits;

class Codegen
{
    const ADDITIONAL_ALIASES = [
        'Dozen(s)' => ['dozen(s)', 'dozen'],
        'Item(s)' => ['item(s)', 'item'],
        'unit' => [''],
    ];
    public static function run()
    {
        (new self)->generate();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function generate()
    {
        $rows = json_decode(file_get_contents(__DIR__."/resources/units.json"), true);
        $properties = $propertyRegistry = $unitGroups = $ugRegistry = $units = $unitRegistry = [];
        foreach ($rows as $row) {
            $property = $row["PROPERTY"];
            $propertyConst = $this->constName($property);
            $properties[$propertyConst] = $property;

            $ug = $row["UNIT_GROUP"];
            $ug = str_replace("Units of ", "", $ug);
            $ugConst = $this->constName($ug);
            $unitGroups[$ugConst] = $ug;
            $propertyRegistry[$propertyConst] = $ugConst;

            $canonicalName = $row["NAME"];
            $names = array_map("trim", explode(";", $row["SYNONYMS"]));
            $names = array_filter($names);
            $names = array_merge(self::ADDITIONAL_ALIASES[$canonicalName] ?? []);
            array_unshift($names, $canonicalName);
            $unitConst = $this->constName($row["DESCRIPTION"]);
            $units[$unitConst] = (float)$row["CONVERSION_FACTOR"];

            if ($row["REF"]) {
                $ugRegistry[$ugConst] = $canonicalName;
            }

            foreach ($names as $name) {
                $unitRegistry[$name] = [$ugConst, $unitConst];
            }
        }

        echo "PROPERTIES:\n";
        foreach ($properties as $const => $name) {
            echo "const $const = '$name';\n";
        }
        echo "\n";

        echo "PROPERTY REGISTRY:\n";
        echo "const REGISTRY = [\n";
        foreach ($propertyRegistry as $property => $group) {
            echo "    self::$property => UnitGroup::$group,\n";
        }
        echo "];\n";

        echo "UNIT GROUPS:\n";
        foreach ($unitGroups as $const => $name) {
            echo "const $const = '$name';\n";
        }
        echo "\n";

        echo "UNIT GROUP REGISTRY:\n";
        echo "const REGISTRY = [\n";
        foreach ($ugRegistry as $group => $unit) {
            echo "    self::$group => '$unit',\n";
        }
        echo "];\n";

        echo "UNITS:\n";
        foreach ($units as $const => $f) {
            echo "const $const = $f;\n";
        }
        echo "\n";

        echo "UNIT REGISTRY:\n";
        echo "const REGISTRY = [\n";
        foreach ($unitRegistry as $unit => $spec) {
            [$group, $unitConst] = $spec;
            echo "    '$unit' => [UnitGroup::$group, self::$unitConst],\n";
        }
        echo "];\n";
    }
    private function constName(string $name) {
        $name = trim($name);
        $name = str_replace(["*", "/", " "], ["_TIMES_", "_PER_", "_"], $name);
        $name = strtoupper($name);
        $name = preg_replace("/[^A-Z0-9_]/", "", $name);
        return preg_replace("/_+/", "_", $name);
    }
}