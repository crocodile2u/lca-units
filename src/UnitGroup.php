<?php
declare(strict_types=1);

namespace LCAUnits;

class UnitGroup
{
    const ITEMS = 'items';
    const LENGTH_TIMES_AREA_PER_TIME = 'length*area/time';
    const ITEMS_TIMES_LENGTH = 'items*length';
    const VOLUME_TIMES_TIME = 'volume*time';
    const LENGTH_TIMES_AREA = 'length*area';
    const MASS_TIMES_TIME = 'mass*time';
    const GROUNDWATER_REPLENISHMENT_TRANSF = 'groundwater replenishment (transf.)';
    const VOLUME_TIMES_LENGTH = 'volume*length';
    const MASS_TIMES_LENGTH = 'mass*length';
    const LENGTH = 'length';
    const PERSON_TRANSPORT = 'person transport';
    const MOLE_TIMES_AREA_TIMES_TIME_PER_MASS = 'mole*area*time/mass';
    const MASS = 'mass';
    const RADIOACTIVITY = 'radioactivity';
    const AREA = 'area';
    const AREA_TIMES_TIME = 'area*time';
    const VOLUME = 'volume';
    const MOLE_TIMES_AREA_PER_MASS = 'mole*area/mass';
    const LENGTH_TIMES_TIME = 'length*time';
    const TIME = 'time';
    const ENERGY_PER_MASS_TIMES_TIME = 'energy/mass*time';
    const MECHANICAL_FILTRATION_OCC = 'mechanical filtration (occ.)';
    const ENERGY_PER_AREA_TIMES_TIME = 'energy/area*time';
    const VEHICLE_TRANSPORT = 'vehicle transport';
    const ENERGY = 'energy';
    const MASS_PER_TIME = 'mass/time';

    const REGISTRY = [
        self::ITEMS => 'Item(s)',
        self::LENGTH_TIMES_AREA_PER_TIME => 'cm*m2/d',
        self::ITEMS_TIMES_LENGTH => 'Items*km',
        self::VOLUME_TIMES_TIME => 'm3*a',
        self::LENGTH_TIMES_AREA => 'mm*m2',
        self::MASS_TIMES_TIME => 'kg*a',
        self::GROUNDWATER_REPLENISHMENT_TRANSF => '(mm*m2)/a',
        self::VOLUME_TIMES_LENGTH => 'm3*km',
        self::MASS_TIMES_LENGTH => 't*km',
        self::LENGTH => 'm',
        self::PERSON_TRANSPORT => 'p*km',
        self::MOLE_TIMES_AREA_TIMES_TIME_PER_MASS => '(cmol*m2*a)/kg',
        self::MASS => 'kg',
        self::RADIOACTIVITY => 'kBq',
        self::AREA => 'm2',
        self::AREA_TIMES_TIME => 'm2*a',
        self::VOLUME => 'm3',
        self::MOLE_TIMES_AREA_PER_MASS => '(cmol*m2)/kg',
        self::LENGTH_TIMES_TIME => 'm*a',
        self::TIME => 'd',
        self::ENERGY_PER_MASS_TIMES_TIME => 'MJ/kg*d',
        self::MECHANICAL_FILTRATION_OCC => 'cm*m3',
        self::ENERGY_PER_AREA_TIMES_TIME => 'kWh/m2*d',
        self::VEHICLE_TRANSPORT => 'v*km',
        self::ENERGY => 'MJ',
        self::MASS_PER_TIME => 'kg/a',
    ];

    private string $name;

    public static function resolveRefUnit(string $group): Unit
    {
        return new Unit(self::REGISTRY[$group] ?? "");
    }

    public static function exists(string $name): bool
    {
        return isset(self::REGISTRY[$name]);
    }

    /**
     * UnitGroup constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function isRegistered(): bool
    {
        return self::exists($this->name);
    }

    public function equals(self $other)
    {
        return $this->name === $other->name;
    }

    public function refUnit(): Unit
    {
        return self::resolveRefUnit($this->name);
    }
}