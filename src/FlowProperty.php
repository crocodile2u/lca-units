<?php
declare(strict_types=1);

namespace LCAUnits;

class FlowProperty
{
    const NUMBER_OF_ITEMS = 'Number of items';
    const MECHANICAL_FILTRATION_TRANSF = 'Mechanical Filtration (Transf.)';
    const ITEMS_TIMES_LENGTH = 'Items*Length';
    const VOLUME_TIMES_TIME = 'Volume*time';
    const GROUNDWATER_REPLENISHMENT_OCC = 'Groundwater Replenishment (Occ.)';
    const MASS_TIMES_TIME = 'Mass*time';
    const GROUNDWATER_REPLENISHMENT_TRANSF = 'Groundwater Replenishment (Transf.)';
    const VOLUME_TIMES_LENGTH = 'Volume*Length';
    const GOODS_TRANSPORT_MASS_TIMES_DISTANCE = 'Goods transport (mass*distance)';
    const LENGTH = 'Length';
    const PERSON_TRANSPORT = 'Person transport';
    const PHYSICOCHEMICAL_FILTRATION_OCC = 'Physicochemical Filtration (Occ.)';
    const MASS = 'Mass';
    const RADIOACTIVITY = 'Radioactivity';
    const AREA = 'Area';
    const AREA_TIMES_TIME = 'Area*time';
    const VOLUME = 'Volume';
    const PHYSICOCHEMICAL_FILTRATION_TRANSF = 'Physicochemical Filtration (Transf.)';
    const LENGTH_TIMES_TIME = 'Length*time';
    const DURATION = 'Duration';
    const ENERGY_PER_MASS_TIMES_TIME = 'Energy/mass*time';
    const MECHANICAL_FILTRATION_OCC = 'Mechanical Filtration (Occ.)';
    const ENERGY_PER_AREA_TIMES_TIME = 'Energy/area*time';
    const VEHICLE_TRANSPORT = 'Vehicle transport';
    const ENERGY = 'Energy';
    const BIOTIC_PRODUCTION_TRANSF = 'Biotic Production (Transf.)';

    const REGISTRY = [
        self::NUMBER_OF_ITEMS => UnitGroup::ITEMS,
        self::MECHANICAL_FILTRATION_TRANSF => UnitGroup::LENGTH_TIMES_AREA_PER_TIME,
        self::ITEMS_TIMES_LENGTH => UnitGroup::ITEMS_TIMES_LENGTH,
        self::VOLUME_TIMES_TIME => UnitGroup::VOLUME_TIMES_TIME,
        self::GROUNDWATER_REPLENISHMENT_OCC => UnitGroup::LENGTH_TIMES_AREA,
        self::MASS_TIMES_TIME => UnitGroup::MASS_TIMES_TIME,
        self::GROUNDWATER_REPLENISHMENT_TRANSF => UnitGroup::GROUNDWATER_REPLENISHMENT_TRANSF,
        self::VOLUME_TIMES_LENGTH => UnitGroup::VOLUME_TIMES_LENGTH,
        self::GOODS_TRANSPORT_MASS_TIMES_DISTANCE => UnitGroup::MASS_TIMES_LENGTH,
        self::LENGTH => UnitGroup::LENGTH,
        self::PERSON_TRANSPORT => UnitGroup::PERSON_TRANSPORT,
        self::PHYSICOCHEMICAL_FILTRATION_OCC => UnitGroup::MOLE_TIMES_AREA_TIMES_TIME_PER_MASS,
        self::MASS => UnitGroup::MASS,
        self::RADIOACTIVITY => UnitGroup::RADIOACTIVITY,
        self::AREA => UnitGroup::AREA,
        self::AREA_TIMES_TIME => UnitGroup::AREA_TIMES_TIME,
        self::VOLUME => UnitGroup::VOLUME,
        self::PHYSICOCHEMICAL_FILTRATION_TRANSF => UnitGroup::MOLE_TIMES_AREA_PER_MASS,
        self::LENGTH_TIMES_TIME => UnitGroup::LENGTH_TIMES_TIME,
        self::DURATION => UnitGroup::TIME,
        self::ENERGY_PER_MASS_TIMES_TIME => UnitGroup::ENERGY_PER_MASS_TIMES_TIME,
        self::MECHANICAL_FILTRATION_OCC => UnitGroup::MECHANICAL_FILTRATION_OCC,
        self::ENERGY_PER_AREA_TIMES_TIME => UnitGroup::ENERGY_PER_AREA_TIMES_TIME,
        self::VEHICLE_TRANSPORT => UnitGroup::VEHICLE_TRANSPORT,
        self::ENERGY => UnitGroup::ENERGY,
        self::BIOTIC_PRODUCTION_TRANSF => UnitGroup::MASS_PER_TIME,
    ];

    const FREQUENTLY_USED = [
        self::MASS,
        self::ENERGY,
        self::GOODS_TRANSPORT_MASS_TIMES_DISTANCE,
        self::DURATION,
        self::VOLUME,
        self::AREA,
        self::NUMBER_OF_ITEMS,
    ];

    private string $name;

    public static function resolveUnitGroup(string $property): UnitGroup
    {
        return new UnitGroup(self::REGISTRY[$property] ?? "");
    }

    public static function names(): array
    {
        $ret = array_merge(self::FREQUENTLY_USED, array_keys(self::REGISTRY));
        // array_flip(array_flip($ret)) removes duplicates and keeps the order:
        // [1, 2, 1, 3, 2, 1, 5, 6] --> [1, 2, 3, 5, 6]
        // array_unique() might change the original order, also it is slower
        return array_values(array_flip(array_flip($ret)));
    }

    /**
     * @return FlowPropertySpec[]
     */
    public static function allSpecifications(): array
    {
        $unitNames = Unit::unitNamesByGroups();
        $ret = [];
        foreach (self::names() as $name) {
            $unitGroupName = self::REGISTRY[$name];
            $ret[] = new FlowPropertySpec($name, $unitGroupName, ...$unitNames[$unitGroupName]);
        }
        return $ret;
    }

    /**
     * FlowProperty constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function unitGroup(): UnitGroup
    {
        return self::resolveUnitGroup($this->name);
    }
}