<?php
declare(strict_types=1);

namespace LCAUnits;

class Quantity implements \JsonSerializable
{
    private float $value;
    private Unit $unit;

    public static function parse(string $value): self
    {
        $value = trim($value);
        if ($value === "") {
            return new self(1, new Unit(""));
        }
        if (is_numeric($value)) {
            return new self(floatval($value), new Unit(""));
        }
        $success = sscanf($value, "%f%s", $n, $unit);
        if ($success) {
            return new self($n, new Unit($unit));
        }

        return new self(1, new Unit($value));
    }

    /**
     * Quantity constructor.
     * @param float $value
     * @param Unit $unit
     */
    public function __construct(float $value, Unit $unit)
    {
        $this->value = $value;
        $this->unit = $unit;
    }

    public function value(): float
    {
        return $this->value;
    }

    public function unit(): Unit
    {
        return $this->unit;
    }

    public function add(self $addendum): self
    {
        return new self($this->value + $addendum->to($this->unit)->value, clone $this->unit);
    }

    public function mul(self $other): self
    {
        $unit = $this->nameOptionallyEnclosed($this->unit->name());
        if ($this->unit->group()->equals($other->unit->group())) {
            $resUnit = $unit ? "{$unit}*{$unit}" : "";
            return new self(
                $this->value * $other->to($this->unit)->value,
                new Unit($resUnit)
            );
        }

        $otherUnit = $this->nameOptionallyEnclosed($other->unit->name());
        if ($unit === "") {
            $resUnit = $otherUnit;
        } elseif ($otherUnit === "") {
            $resUnit = $unit;
        } else {
            $resUnit = "{$unit}*{$otherUnit}";
        }
        return new self(
            $this->value * $other->value,
            new Unit($resUnit)
        );
    }

    public function div(self $divider): self
    {
        if ($this->unit->group()->equals($divider->unit->group())) {
            return new self(
                $this->value / $divider->to($this->unit)->value,
                new Unit("")
            );
        }

        $dividerUnitName = $this->nameOptionallyEnclosed($divider->unit->name());
        if ($this->unit->name() === "") {
            $resUnit = "1/$dividerUnitName";
        } elseif ($dividerUnitName === "") {
            $resUnit = $this->unit->name();
        } else {
            $resUnit = "{$this->unit->name()}/{$dividerUnitName}";
        }
        return new self(
            $this->value / $divider->value,
            new Unit($resUnit)
        );
    }

    public function toBaseUnit(): self
    {
        return $this->to($this->unit->group()->refUnit());
    }

    public function to(Unit $targetUnit): self
    {
        if ($targetUnit->equals($this->unit)) {
            return new self($this->value, clone $targetUnit);
        }
        $this->assertSameUnitGroup($targetUnit);
        return new self($this->value * $this->unit->factor() / $targetUnit->factor(), clone $targetUnit);
    }

    private function assertSameUnitGroup(Unit $other): void
    {
        if (!$other->group()->equals($this->unit->group())) {
            throw new \InvalidArgumentException(
                sprintf("unit groups mismatch for '%s' and '%s'", $this->unit->name(), $other->name())
            );
        }
    }

    private function nameOptionallyEnclosed(string $unit): string
    {
        if (strpos($unit, '/') !== false) {
            return "($unit)";
        }
        return $unit;
    }

    public function jsonSerialize()
    {
        return [
            "unit" => $this->unit,
            "value" => $this->value,
        ];
    }
}