# lca-units

PHP library for physical Units of Measure that are used in LCA (Life Cycle Assessment). Units, Unit groups, Flow properties recognized by professional LCA software. Unit conversions are supported.