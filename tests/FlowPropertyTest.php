<?php

namespace Tests;

use LCAUnits\FlowProperty;
use PHPUnit\Framework\TestCase;

class FlowPropertyTest extends TestCase
{

    public function testNames()
    {
        $props = FlowProperty::names();
        $this->assertSame(count(FlowProperty::REGISTRY), count($props));
        foreach (FlowProperty::FREQUENTLY_USED as $i => $name) {
            $this->assertSame($name, $props[$i]);
        }
    }
}
