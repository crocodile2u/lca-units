<?php

namespace Tests;

use LCAUnits\Unit;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{

    public function testUnitsByGroups()
    {
        $groups = Unit::unitsByGroups();
        $this->assertIsArray($groups);
        foreach ($groups as $group => $units) {
            $this->assertIsString($group);
            $this->assertIsArray($units);
            foreach ($units as $unit) {
                $this->assertInstanceOf(Unit::class, $unit);
            }
        }
    }

    public function testUnitNamesByGroups()
    {
        $groups = Unit::unitNamesByGroups();
        $this->assertIsArray($groups);
        foreach ($groups as $group => $units) {
            $this->assertIsString($group);
            $this->assertIsArray($units);
            foreach ($units as $unit) {
                $this->assertIsString($unit);
            }
        }
    }
}
