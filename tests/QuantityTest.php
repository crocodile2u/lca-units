<?php

namespace Tests;

use http\Exception\InvalidArgumentException;
use LCAUnits\Quantity;
use LCAUnits\Unit;

class QuantityTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param string $s
     * @param float $val
     * @param string $unit
     * @dataProvider provideParseData
     */
    public function testParse(string $s, float $val, string $unit)
    {
        $q = Quantity::parse($s);
        $this->assertSame($val, $q->value());
        $this->assertSame($unit, $q->unit()->name());
    }

    public function provideParseData()
    {
        return [
            "empty string" => ["", 1.0, ""],
            "int number" => ["10", 10, ""],
            "int number, negative" => ["-10", -10, ""],
            "zero" => ["0", 0, ""],
            "negative zero" => ["-0", 0, ""],
            "negative zero float" => ["-0.0", 0.0, ""],
            "float number" => ["10.45", 10.45, ""],
            "float number starting with dot" => [".45", .45, ""],
            "float number, negative" => ["-10.45", -10.45, ""],
            "float number, scientific" => ["1.45E2", 1.45E2, ""],
            "float number, scientific, negative power" => ["1.45E-2", 1.45E-2, ""],
            "float number, scientific, negative" => ["-1.45E2", -1.45E2, ""],
            "float number, scientific, negative, negative power" => ["-1.45E-2", -1.45E-2, ""],

            "kg, int number" => ["10kg", 10, "kg"],
            "kg, int number, negative" => ["-10kg", -10, "kg"],
            "kg, float number" => ["10.45kg", 10.45, "kg"],
            "kg, float number starting with dot" => [".45kg", .45, "kg"],
            "kg, float number starting with dot, negative" => ["-.45kg", -.45, "kg"],
            "kg, float number zero, negative" => ["-0kg", 0, "kg"],
            "kg, float number, negative" => ["-10.45kg", -10.45, "kg"],
            "kg, float number, scientific" => ["1.45E2kg", 1.45E2, "kg"],
            "kg, float number, scientific, negative power" => ["1.45E-2kg", 1.45E-2, "kg"],
            "kg, float number, scientific, negative" => ["-1.45E2kg", -1.45E2, "kg"],
            "kg, float number, scientific, negative, negative power" => ["-1.45E-2kg", -1.45E-2, "kg"],

            "kg, with whitespace, int number" => ["10 kg", 10, "kg"],
            "kg, with whitespace, int number, negative" => ["-10 kg", -10, "kg"],
            "kg, with whitespace, float number" => ["10.45 kg", 10.45, "kg"],
            "kg, with whitespace, float number, negative" => ["-10.45 kg", -10.45, "kg"],
            "kg, with whitespace, float number, scientific" => ["1.45E2 kg", 1.45E2, "kg"],
            "kg, with whitespace, float number, scientific, negative power" => ["1.45E-2 kg", 1.45E-2, "kg"],
            "kg, with whitespace, float number, scientific, negative" => ["-1.45E2 kg", -1.45E2, "kg"],
            "kg, with whitespace, float number, scientific, negative, negative power" => ["-1.45E-2 kg", -1.45E-2, "kg"],

            "multiple whitespace" => ["10     kg", 10, "kg"],

            "only unit" => ["kg", 1, "kg"],
        ];
    }

    /**
     * @param string $s
     * @param string $unit
     * @param float $expected
     * @param bool $expectException
     * @dataProvider providerConvertData
     */
    public function testConvert(string $s, string $unit, float $expected, $expectException = false)
    {
        if ($expectException) {
            $this->expectException(\InvalidArgumentException::class);
        }
        $converted = Quantity::parse($s)->to(new Unit($unit));
        $this->assertSame($expected, $converted->value());
        $this->assertSame($unit, $converted->unit()->name());
    }

    public function providerConvertData()
    {
        return [
            "kg -> kg" => ["1kg", "kg", 1, false],
            "kg -> g" => ["1kg", "g", 1000, false],
            "l -> m3" => ["1000l", "m3", 1, false],
            "p*mi -> p*km" => ["1p*mi", "p*km", 1.6, false],
            "m -> m2" => ["1m", "m2", 0, true],
        ];
    }

    /**
     * @param string $s
     * @param string $expectedUnit
     * @param float $expectedValue
     * @dataProvider providerConvertToBaseUnitData
     */
    public function testConvertToBaseUnit(string $s, string $expectedUnit, float $expectedValue)
    {
        $converted = Quantity::parse($s)->toBaseUnit();
        $this->assertSame($expectedUnit, $converted->unit()->name());
        $this->assertSame($expectedValue, $converted->value());
    }

    public function providerConvertToBaseUnitData()
    {
        return [
            "g -> kg" => ["1000g", "kg", 1],
            "item -> Item(s)" => ["5item", "Item(s)", 5],
            "no unit -> Item(s)" => ["5", "Item(s)", 5],
        ];
    }

    /**
     * @param string $a
     * @param string $b
     * @param string $expectedUnit
     * @param float $expectedValue
     * @param bool $expectException
     * @dataProvider provideAddData
     */
    public function testAdd(string $a, string $b, string $expectedUnit, float $expectedValue, bool $expectException)
    {
        if ($expectException) {
            $this->expectException(\InvalidArgumentException::class);
        }
        $sum = Quantity::parse($a)->add(Quantity::parse($b));
        $this->assertSame($expectedUnit, $sum->unit()->name());
        $this->assertSame($expectedValue, $sum->value());
    }

    public function provideAddData()
    {
        return [
            "kg + kg" => ["1kg", "1kg", "kg", 2, false],
            "kg + g" => ["1kg", "100g", "kg", 1.1, false],
            "kg + m3" => ["1kg", "1m3", "", 0, true],
        ];
    }

    /**
     * @param string $a
     * @param string $b
     * @param string $expectedUnit
     * @param float $expectedValue
     * @dataProvider provideMulData
     */
    public function testMul(string $a, string $b, string $expectedUnit, float $expectedValue)
    {
        $res = Quantity::parse($a)->mul(Quantity::parse($b));
        $this->assertSame($expectedUnit, $res->unit()->name());
        $this->assertSame($expectedValue, $res->value());
    }

    public function provideMulData()
    {
        return [
            "no unit * no unit" => ["5", "5", "", 25],
            "unit * no unit" => ["5unit", "5", "unit*unit", 25],
            "kg * m" => ["5kg", "5m", "kg*m", 25],
            "kg * g" => ["5kg", "1000g", "kg*kg", 5],
            "kg * no unit" => ["5kg", "5", "kg", 25],
            "no unit * kg" => ["5", "5kg", "kg", 25],
            "complex" => ["5m3", "5unit/kg", "m3*(unit/kg)", 25],
        ];
    }

    /**
     * @param string $a
     * @param string $b
     * @param string $expectedUnit
     * @param float $expectedValue
     * @dataProvider provideDivData
     */
    public function testDiv(string $a, string $b, string $expectedUnit, float $expectedValue)
    {
        $res = Quantity::parse($a)->div(Quantity::parse($b));
        $this->assertSame($expectedUnit, $res->unit()->name());
        $this->assertSame($expectedValue, $res->value());
    }

    public function provideDivData()
    {
        return [
            "no unit / no unit" => ["5", "5", "", 1],
            "unit / no unit" => ["5unit", "5", "", 1],
            "kg / m" => ["5kg", "5m", "kg/m", 1],
            "kg / g" => ["5kg", "1000g", "", 5],
            "kg / no unit" => ["5kg", "5", "kg", 1],
            "no unit / kg" => ["5", "5kg", "1/kg", 1],
            "unit / kg" => ["5unit", "5kg", "unit/kg", 1],
            "complex" => ["5m3", "5unit/kg", "m3/(unit/kg)", 1],
        ];
    }
}